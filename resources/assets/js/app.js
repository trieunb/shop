
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import 'es6-promise/auto'
window.Vue = require('vue');
import BootstrapVue from 'bootstrap-vue'
import Vuex         from 'vuex'
import AmCharts     from 'amcharts3'
import AmSerial     from 'amcharts3/amcharts/serial'
import AmPie        from 'amcharts3/amcharts/pie'
import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect);

Vue.use(Vuex)
Vue.use(BootstrapVue);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
import router from './router';

const app = new Vue({
    el: '#root',
    router
});
