const routes = [
    {
        path: '/admin',
        component: require('../layouts/admin/admin'),
        redirect: '/admin',
        children: [
            //Route Dashboard
            {
                path: '/admin',
                component: require('../modules/admin/dashboard/index'),
                name: 'dashboard',
            },
            //Route User
            {
                path: '/admin/user',
                component: require('../modules/admin/user/index'),
                name: 'admin.user',
            },
            {
                path: '/admin/user/create',
                component: require('../modules/admin/user/create'),
                name: 'admin.user.create',
            },
            {
                path: '/admin/user/edit',
                component: require('../modules/admin/user/edit'),
                name: 'admin.user.edit',
            },
            //Route Product
            {
                path: '/admin/product',
                component: require('../modules/admin/product/index'),
                name: 'admin.product',
            },
            {
                path: '/admin/product/create',
                component: require('../modules/admin/product/create'),
                name: 'admin.product.create',
            },
            {
                path: '/admin/product/edit',
                component: require('../modules/admin/product/edit'),
                name: 'admin.product.edit',
            },
            //Route Category
            {
                path: '/admin/category',
                component: require('../modules/admin/category/index'),
                name: 'admin.category',
            },
            {
                path: '/admin/category/create',
                component: require('../modules/admin/category/create'),
                name: 'admin.category.create',
            },
            {
                path: '/admin/category/edit',
                component: require('../modules/admin/category/edit'),
                name: 'admin.category.edit',
            },
            //Route Customer
            {
                path: '/admin/customer',
                component: require('../modules/admin/customer/index'),
                name: 'admin.customer',
            },
            {
                path: '/admin/customer/create',
                component: require('../modules/admin/customer/create'),
                name: 'admin.customer.create',
            },
            {
                path: '/admin/customer/edit',
                component: require('../modules/admin/customer/edit'),
                name: 'admin.customer.edit',
            },
            //Route Order
            {
                path: '/admin/order',
                component: require('../modules/admin/order/index'),
                name: 'admin.order',
            },
            {
                path: '/admin/order/create',
                component: require('../modules/admin/order/create'),
                name: 'admin.order.create',
            },
            //Route Transport
            {
                path: '/admin/transport',
                component: require('../modules/admin/transport/index'),
                name: 'admin.transport',
            },
            {
                path: '/admin/transport/create',
                component: require('../modules/admin/transport/create'),
                name: 'admin.transport.create',
            },
            {
                path: '/admin/transport/edit',
                component: require('../modules/admin/transport/edit'),
                name: 'admin.transport.edit',
            },
            //Route Setting
            {
                path: '/admin/setting',
                component: require('../modules/admin/setting/index'),
                name: 'admin.setting',
            },
            //Route Table
            {
                path: '/admin/table/simple',
                component: require('../modules/admin/common/table/simple/index'),
                name: 'table.simple',
            },
            {
                path: '/admin/table/advanced',
                component: require('../modules/admin/common/table/advanced/index'),
                name: 'table.advanced',
            },
            //Route form
            {
                path: '/admin/form/element',
                component: require('../modules/admin/common/form/element/index'),
                name: 'form.element',
            },
            {
                path: '/admin/form/validate',
                component: require('../modules/admin/common/form/validate/index'),
                name: 'form.validate',
            },
            {
                path: '/admin/form/editor',
                component: require('../modules/admin/common/form/editor/index'),
                name: 'form.editor',
            },
            {
                path: '/admin/form/dropdown',
                component: require('../modules/admin/common/form/dropdown/index'),
                name: 'form.dropdown',
            },
            {
                path: '/admin/form/checkbox',
                component: require('../modules/admin/common/form/checkbox/index'),
                name: 'form.checkbox',
            }
        ]
    },
    //router web
    {
        path: '/',
        component: require('../layouts/shop/shop'),
        redirect: '/',
        children: [
            {
                path: '/',
                component: require('../modules/shop/home/index'),
                name: 'home',
            }
        ]
    },
    //router auth
    {
        path: '/admin/login',
        component: require('../layouts/auth/admin/login'),
        name: 'admin.login',
    },
    {
        path: '/admin/register',
        component: require('../layouts/auth/admin/register'),
        name: 'admin.register',
    },
];
export default routes;
